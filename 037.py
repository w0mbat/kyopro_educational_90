# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
from typing import Iterable, List
from collections import deque
def main():
    input = sys.stdin.readline
    W, N = map(int, input().split())
    LRV = [tuple(map(int, input().split())) for _ in range(N)]
    dp = [[-1] * (W + 1) for _ in range(N + 1)]
    dp[0][0] = 0
    for i, (l, r, v) in enumerate(LRV):
        smx = slide_maximum(dp[i], r - l + 1)
        for w in range(W + 1):
            dp[i + 1][w] = dp[i][w]
            if w >= l and smx[w - l] >= 0:
                dp[i + 1][w] = max(dp[i + 1][w], smx[w - l] + v)
    print(dp[N][W])

def slide_maximum(A: Iterable[int], width=0, f=lambda x, y: x < y) -> List[int]:
    res = []
    q = deque()
    for i, a in enumerate(A):
        while width and q and q[0] <= i - width: q.popleft()
        while q and f(A[q[-1]], a): q.pop()
        q.append(i)
        res.append(A[q[0]])
    return res

if __name__ == '__main__':
    main()

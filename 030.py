# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
N, K = map(int, input().split())
if K == 1:
    print(N - 1)
    exit()
ans = 0
A = [0] * (N + 1)
for n in range(2, N + 1):
    a = A[n]
    if a > 0:
        if a >= K:
            ans += 1
        continue
    for m in range(n + n, N + 1, n):
        A[m] += 1
print(ans)

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
MOD = 10**9 + 7
def main():
    input = sys.stdin.readline
    N, B, K = map(int, input().split())
    *C, = map(int, input().split())

    def f(left, right, shift):
        res = [0] * B
        for lm in range(B):
            for rm in range(B):
                nm = (lm * shift + rm) % B
                res[nm] = (res[nm] + left[lm] * right[rm]) % MOD
        return res

    t = [0] * B

    # 1桁のとき
    for c in C: t[c % B] += 1

    ans = [0] * B
    ans[0] = 1
    shift = 1
    for i in range(N.bit_length()):
        shift = pow(10, 1 << i, B)
        if N & (1 << i):
            ans = f(ans, t, shift)
        t = f(t, t, shift)
    print(ans[0])

if __name__ == '__main__':
    main()

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
def main():
    input = sys.stdin.readline
    N, K = map(int, input().split())
    XY = [tuple(map(int, input().split())) for _ in range(N)]

    def max_d(S):
        res = 0
        I = [i for i in range(N) if S & (1 << i)]
        for i in range(len(I)):
            for j in range(i):
                res = max(res, d(XY[I[i]], XY[I[j]]))
        return res

    DS = [max_d(s) for s in range(1 << N)]
    INF = 10**18
    M = (1 << N) - 1
    dp = [[INF] * (1 << N) for _ in range(K + 1)]
    dp[0][0] = 0
    for k in range(K):
        for S in range(1 << N):
            if dp[k][S] == INF: continue
            C = ~S & M
            a = C
            while a:
                dp[k + 1][S | a] = min(dp[k + 1][S | a], max(dp[k][S], DS[a]))
                a = (a - 1) & C
    print(dp[K][M])

def d(a, b):
    x1, y1 = a
    x2, y2 = b
    return (x1 - x2)**2 + (y1 - y2)**2

if __name__ == '__main__':
    main()

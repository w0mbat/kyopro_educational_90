# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
K = int(input())
if K % 9:
    print(0)
    exit()
MOD = 10**9 + 7
dp = [0] * (K + 1)
dp[0] = 1
for k in range(1, K + 1):
    res = 0
    for d in range(1, 10):
        if k - d < 0: break
        res += dp[k - d]
        res %= MOD
    dp[k] = res
print(dp[K])

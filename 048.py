# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
def main():
    input = sys.stdin.readline
    N, K = map(int, input().split())
    P = []
    for _ in range(N):
        a, b = map(int, input().split())
        P.append(b)
        P.append(a - b)
    P.sort(reverse=True)
    print(sum(P[:K]))

if __name__ == '__main__':
    main()

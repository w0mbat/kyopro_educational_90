# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from collections import Counter
import sys
def main():
    input = sys.stdin.readline
    _ = int(input())
    *A, = map(lambda x: int(x) % 46, input().split())
    *B, = map(lambda x: int(x) % 46, input().split())
    *C, = map(lambda x: int(x) % 46, input().split())
    CA = Counter(A)
    CB = Counter(B)
    CC = Counter(C)
    ans = 0
    for i in range(46):
        for j in range(46):
            k = (46 - i - j) % 46
            ans += CA[i] * CB[j] * CC[k]
    print(ans)

if __name__ == '__main__':
    main()

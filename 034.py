# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
def main():
    input = sys.stdin.readline
    N, K = map(int, input().split())
    *A, = map(int, input().split())
    bit = BinaryIndexedTree(N)
    last = dict()
    r = 0
    ans = 0
    for l in range(N):
        while r < N:
            ar = A[r]
            if ar in last:
                li = last[ar]
                bit.add(li, -1)
            bit.add(r, 1)
            last[ar] = r
            if bit.query(N - 1) - bit.query(l - 1) > K: break
            r += 1
        ans = max(ans, r - l)
    print(ans)

class BinaryIndexedTree:
    __slots__ = ('__n', '__d', '__f', '__id')

    def __init__(self, n=None, f=lambda x, y: x + y, identity=0, initial_values=None):
        assert(n or initial_values)
        self.__f, self.__id, = f, identity
        self.__n = len(initial_values) if initial_values else n
        self.__d = [identity] * (self.__n + 1)
        if initial_values:
            for i, v in enumerate(initial_values): self.add(i, v)

    def add(self, i, v):
        n, f, d = self.__n, self.__f, self.__d
        i += 1
        while i <= n:
            d[i] = f(d[i], v)
            i += -i & i

    def query(self, r):
        res, f, d = self.__id, self.__f, self.__d
        r += 1
        while r:
            res = f(res, d[r])
            r -= -r & r
        return res

    def bisect(self, func):
        '''func()がFalseになるもっとも左のindexを探す
        '''
        n, f, d, v = self.__n, self.__f, self.__d, self.__id
        x, i = 0, 1 << (n.bit_length() - 1)
        while i > 0:
            if x + i <= n and func(f(v, d[x + i])): v, x = f(v, d[x + i]), x + i
            i >>= 1
        return x

if __name__ == '__main__':
    main()

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
def main():
    input = sys.stdin.readline
    N = int(input())
    S = set()
    for i in range(1, N + 1):
        s = input().rstrip()
        if s in S: continue
        print(i)
        S.add(s)

if __name__ == '__main__':
    main()

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from bisect import bisect_right
import sys
def main():
    input = sys.stdin.readline
    N, K, P = map(int, input().split())
    *A, = map(int, input().split())

    def f(B):
        M = len(B)
        res = [[] for _ in range(K + 1)]
        res[0].append(0)
        for p in range(1, 1 << M):
            pc = popcount(p)
            if pc > K: continue
            s = 0
            for i in range(M):
                if p & (1 << i):
                    s += B[i]
            res[pc].append(s)
        for r in res:
            r.sort()
        return res

    ans = 0
    H = N // 2
    P1 = f(A[:H])
    P2 = f(A[H:])
    for c1 in range(K + 1):
        c2 = K - c1
        for p1 in P1[c1]:
            ans += bisect_right(P2[c2], P - p1)
    print(ans)

def popcount(x):
    x = (x & 0x5555555555555555) + (x >> 1 & 0x5555555555555555)
    x = (x & 0x3333333333333333) + (x >> 2 & 0x3333333333333333)
    x = x + (x >> 4) & 0x0f0f0f0f0f0f0f0f
    x += x >> 8
    x += x >> 16
    x += x >> 32
    return x & 0x7f

if __name__ == '__main__':
    main()

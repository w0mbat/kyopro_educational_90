import sys
Q, *TX = map(int, sys.stdin.buffer.read().split())
A = [0] * (Q * 2 + 1)
l = Q
r = l + 1
for t, x in zip(*[iter(TX)] * 2):
    if t == 1:
        A[l] = x
        l -= 1
    elif t == 2:
        A[r] = x
        r += 1
    else:
        print(A[l + x])

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
from heapq import heappush, heappop
INF = 10**18
def main():
    input = sys.stdin.readline
    N, M = map(int, input().split())
    G = [[] for _ in range(N)]
    for _ in range(M):
        x, y, c = map(int, input().split())
        x, y = x - 1, y - 1
        G[x].append((y, c))
        G[y].append((x, c))

    def f(s):
        q = [(0, s)]
        dist = [INF] * N
        dist[s] = 0
        while q:
            d, v = heappop(q)
            if dist[v] != d: continue
            for nv, c in G[v]:
                nd = d + c
                if dist[nv] <= nd: continue
                dist[nv] = nd
                heappush(q, (nd, nv))
        return dist

    D1 = f(0)
    Dn = f(N - 1)
    for i in range(N):
        print(D1[i] + Dn[i])

if __name__ == '__main__':
    main()

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
def main():
    input = sys.stdin.readline
    N, L = map(int, input().split())
    dp = [0] * (N + 1)
    dp[0] = 1
    MOD = 10**9 + 7
    for i in range(1, N + 1):
        dp[i] += dp[i - 1]
        if i >= L:
            dp[i] += dp[i - L]
        dp[i] %= MOD
    print(dp[N])

if __name__ == '__main__':
    main()

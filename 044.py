# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
def main():
    input = sys.stdin.readline
    N, Q = map(int, input().split())
    *A, = map(int, input().split())
    shift = 0
    for _ in range(Q):
        t, x, y = map(int, input().split())
        x, y = x - 1, y - 1
        x, y = (x - shift) % N, (y - shift) % N
        if t == 1:
            A[x], A[y] = A[y], A[x]
        elif t == 2:
            shift += 1
        else:
            print(A[x])

if __name__ == '__main__':
    main()

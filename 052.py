# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
def main():
    input = sys.stdin.readline
    N = int(input())
    A = [tuple(map(int, input().split())) for _ in range(N)]
    MOD = 10**9 + 7

    def f(x):
        if x == N: return 1
        res = sum(A[x]) * f(x + 1)
        res %= MOD
        return res

    print(f(0))

if __name__ == '__main__':
    main()

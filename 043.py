# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from collections import deque
import sys
def main():
    input = sys.stdin.readline
    H, W = map(int, input().split())
    si, sj = map(lambda x: int(x) - 1, input().split())
    gi, gj = map(lambda x: int(x) - 1, input().split())
    D = {'#': 1, '.': 0}
    C = tuple(tuple(map(lambda x: D[x], input().rstrip())) for _ in range(H))
    INF = 10**18
    dp = [INF] * (H * W * 4)
    q = deque()
    for d in range(4):
        dp[H * W * d + W * si + sj] = 0
        q.append((H * W * d + W * si + sj, 0))
    dij = ((1, 0), (-1, 0), (0, 1), (0, -1))
    while q:
        v, c = q.popleft()
        if dp[v] != c: continue
        d = v // (H * W)
        i = (v % (H * W)) // W
        j = v % W
        di, dj = dij[d]
        ni, nj = i + di, j + dj
        nv = H * W * d + W * ni + nj
        if 0 <= ni < H and 0 <= nj < W:
            if C[ni][nj] == 0:
                if dp[nv] > c:
                    dp[nv] = c
                    q.appendleft((nv, c))
        for nd in range(4):
            if nd == d: continue
            nv = H * W * nd + W * i + j
            if dp[nv] > c + 1:
                dp[nv] = c + 1
                q.append((nv, c + 1))
    ans = INF
    for d in range(4):
        ans = min(ans, dp[H * W * d + W * gi + gj])
    print(ans)

if __name__ == '__main__':
    main()

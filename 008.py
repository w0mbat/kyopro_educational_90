# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from collections import defaultdict
MOD = 10**9 + 7
N = int(input())
S = input()
C = defaultdict(int)
ATCODER = 'atcoder'
dp = [0] * len(ATCODER)
for s in S[::-1]:
    if s in ATCODER:
        i = ATCODER.index(s)
        if i == len(ATCODER) - 1:
            dp[i] += 1
        else:
            dp[i] = (dp[i] + dp[i + 1]) % MOD
print(dp[0])

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
def main(N, G, VV):
    tour, tour_depth, node_in, node_out = euler_tour(G)
    st = SegmentTree(f=min, identity_factory=lambda: 10**18, initial_values=tour_depth)

    def dist(u, v):
        u_in, u_out = node_in[u], node_out[u]
        u_depth = tour_depth[u_in]
        v_in, v_out = node_in[v], node_out[v]
        v_depth = tour_depth[v_in]
        T = [u_in, u_out, v_in, v_out]
        lca_depth = st.query(min(T), max(T))
        return u_depth + v_depth - (lca_depth << 1)

    for V in VV:
        V.sort(key=lambda v: node_in[v])
        d_sum = sum(dist(V[i], V[(i + 1)]) for i in range(len(V) - 1))
        d_sum += dist(V[0], V[-1])
        print(d_sum // 2)

def euler_tour(G, root=0):
    N = len(G)
    tour = []
    tour_depth = []
    node_in = [-1] * N
    node_out = [-1] * N
    node_depth = [-1] * N
    stk = [root]
    push, pop = stk.append, stk.pop
    node_depth[root] = 0
    while stk:
        v = pop()
        d = node_depth[v]
        t = len(tour)
        tour.append(v)
        tour_depth.append(d)
        if node_in[v] < 0:
            node_in[v] = t
            stk_len = len(stk)
            for nv in G[v]:
                if node_in[nv] >= 0: continue
                push(v)
                push(nv)
                node_depth[nv] = d + 1
            if len(stk) == stk_len:
                node_out[v] = t + 1
        else:
            node_out[v] = t + 1
    tour.append(-1)
    tour_depth.append(-1)
    return(tour, tour_depth, node_in, node_out)

class SegmentTree:
    __slots__ = ('__n', '__d', '__f', '__e')

    def __init__(self, n=None, f=max, identity_factory=int, initial_values=None):
        assert(n or initial_values)
        size = n if n else len(initial_values)
        d = [identity_factory() for _ in range(2 * size + 1)]
        self.__n, self.__d, self.__f, self.__e = size, d, f, identity_factory
        if initial_values:
            for i, v in enumerate(initial_values): d[size + i] = v
            for i in range(size - 1, 0, -1): d[i] = f(d[i << 1], d[i << 1 | 1])

    def __repr__(self) -> str:
        n = self.__n
        return f'SegmentTree: {self.__d[n:n+n]}'

    def get_at(self, index):
        return self.__d[index + self.__n]

    def set_at(self, index, new_value):
        i, d, f = index + self.__n, self.__d, self.__f
        if d[i] == new_value: return
        d[i], i = new_value, i >> 1
        while i: d[i], i = f(d[i << 1], d[i << 1 | 1]), i >> 1

    def modify_at(self, index, value):
        self.set_at(index, self.__f(self.__d[index + self.__n], value))

    def query(self, from_inclusive, to_exclusive):
        ans = self.__e()
        if to_exclusive <= from_inclusive: return ans
        l, r, d, f = from_inclusive + self.__n, to_exclusive + self.__n, self.__d, self.__f
        while l < r:
            if l & 1: ans, l = f(ans, d[l]), l + 1
            if r & 1: ans, r = f(d[r - 1], ans), r - 1
            l, r = l >> 1, r >> 1
        return ans

    def query_all(self):
        return self.__d[1]

    def bisect_left(self, func, from_inclusive=None, to_exclusive=None):
        '''func()がTrueになるもっとも左のindexを探す
        '''
        n, f, d, v = self.__n, self.__f, self.__d, self.__e()
        l = (from_inclusive if from_inclusive else 0) + n
        r = (to_exclusive if to_exclusive else n) + n
        h = 0
        while l < r:
            if l & 1:
                nv = f(v, d[l])
                if func(nv): return self.__single_bisect_left(func, l, v)
                v, l = nv, l + 1
            l, r, h = l >> 1, r >> 1, h + 1
        if r == 0: return n
        for i in range(h, 0, -1):
            if ((r + 1) << i <= n + n):
                nv = f(v, d[r])
                if func(nv): return self.__single_bisect_left(func, r, v)
                v, r = nv, r + 1
            r <<= 1
        return n

    def __single_bisect_left(self, func, root, left_val):
        n, f, d = self.__n, self.__f, self.__d
        i, v = root, left_val
        while i < n:
            i <<= 1
            nv = f(v, d[i])
            if not func(nv): i, v = i + 1, nv
        return i - n

if __name__ == '__main__':
    input = sys.stdin.readline
    N = int(input())
    G = [[] for _ in range(N)]
    for _ in range(N - 1):
        x, y = map(int, input().split())
        x, y = x - 1, y - 1
        G[x].append(y)
        G[y].append(x)
    Q = int(input())
    V = []
    for _ in range(Q):
        _, *v = map(lambda x: int(x) - 1, input().split())
        V.append(v)
    main(N, G, V)

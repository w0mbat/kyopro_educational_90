# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
N, B = map(int, input().split())
Num = [-1] * 11
Cm = [0] * 10

ans = 0

def f(n):
    global ans
    mul = 1
    for i in range(n): mul *= Num[i]
    C = [0] * 10
    a = B + mul
    if a > N: return
    while a:
        C[a % 10] += 1
        a //= 10
    if all(Cm[d] == C[d] for d in range(10)):
        ans += 1
    if n == 11: return
    for nn in range(Num[n - 1], 10):
        Num[n] = nn
        Cm[nn] += 1
        f(n + 1)
        Num[n] = -1
        Cm[nn] -= 1

for i in range(1, 10):
    Num[0] = i
    Cm[i] += 1
    f(1)
    Num[0] = -1
    Cm[i] -= 1
if str(B).count('0') > 0 and N >= B: ans += 1
print(ans)

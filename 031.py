# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from functools import reduce
from operator import xor
import sys
def main():
    input = sys.stdin.readline
    _ = int(input())
    *W, = map(int, input().split())
    *B, = map(int, input().split())
    M = 50
    G = [[-1] * (M * M) for _ in range(M + 1)]

    def g(w, b):
        if G[w][b] >= 0:
            return G[w][b]
        gg = set()
        if w > 0:
            gg.add(g(w - 1, b + w))
        for k in range(1, b // 2 + 1):
            gg.add(g(w, b - k))
        res = 0
        while res in gg:
            res += 1
        G[w][b] = res
        return res

    g = reduce(xor, [g(w, b) for w, b in zip(W, B)])
    print('Second' if g == 0 else 'First')

if __name__ == '__main__':
    main()

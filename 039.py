# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from typing import Generator, List, Tuple
import sys
def main():
    input = sys.stdin.readline
    N = int(input())
    T = TreeDPHelper(N)
    for _ in range(N - 1):
        a, b = map(int, input().split())
        a, b = a - 1, b - 1
        T.add_edge(a, b)
    T.dfs()
    dp = [0] * N
    for c, p in T.child_to_parent_connections():
        dp[c] += 1
        if p != c: dp[p] += dp[c]
    ans = 0
    for i in range(1, N):
        ans += dp[i] * (N - dp[i])
    print(ans)

class TreeDPHelper:
    def __init__(self, vertex_num: int) -> None:
        self.N = vertex_num
        self.G = [[] for _ in range(vertex_num)]

    def add_edge(self, u: int, v: int) -> None:
        G = self.G
        G[u].append(v)
        G[v].append(u)

    def dfs(self, root: int = 0) -> None:
        N, G = self.N, self.G
        P = [-1] * N
        O = []
        P[root] = root
        stk = [root]
        push, pop = stk.append, stk.pop
        while stk:
            v = pop()
            O.append(v)
            for nv in G[v]:
                if P[nv] >= 0: continue
                P[nv] = v
                push(nv)
        self.P = P
        self.O = O
        self.R = root

    def leafs(self) -> List[int]:
        N, G, R = self.N, self.G, self.R
        return [i for i in range(N) if len(G[i]) == 1 and i != R]

    def child_to_parent_connections(self) -> Generator[Tuple[int, int], None, None]:
        O, P = self.O, self.P
        for c in O[::-1]:
            yield (c, P[c])

if __name__ == '__main__':
    main()

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from bisect import bisect_left
from cmath import phase, pi
from typing import List
C = 2 * pi
N = int(input())
P: List[complex] = []
for _ in range(N):
    x, y = map(int, input().split())
    P.append(x + y * 1j)
ans = 0
for i in range(N):
    A = sorted((phase(P[j] - P[i]) + pi) * 360 / C for j in range(N) if i != j)
    now = 0
    for Aj in A:
        k = bisect_left(A, Aj + 180)
        r = A[k % len(A)] - Aj
        if r < 0: r += 360
        if r > 180: r = 360 - r
        l = A[k % len(A) - 1] - Aj
        if l < 0: l += 360
        if l > 180: l = 360 - l
        now = max(now, l, r)
    ans = max(ans, now)
print(ans)

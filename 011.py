# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
def main():
    input = sys.stdin.readline
    N = int(input())
    DCS = sorted([tuple(map(int, input().split())) for _ in range(N)], key=lambda x: x[0])
    M = max(d for d, c, s in DCS)
    dp = [[0] * (M + 1) for _ in range(N + 1)]
    for i in range(N):
        d, c, s = DCS[i]
        for j in range(M + 1):
            dp[i + 1][j] = max(dp[i + 1][j], dp[i][j])
            if j + c > d: continue
            dp[i + 1][j + c] = max(dp[i + 1][j + c], dp[i][j] + s)
    print(max(dp[N]))

if __name__ == '__main__':
    main()

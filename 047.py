# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
import sys
def main():
    input = sys.stdin.readline
    N = int(input())
    S = input().rstrip()
    T = input().rstrip()

    R = 3
    P = 10**9 + 7

    RR = [1]
    for _ in range(2 * N):
        RR.append(RR[-1] * 3 % P)

    def hash(L):
        res = [0]
        for l in L:
            res.append((res[-1] * R + l) % P)
        return res

    def h(H, l, r_ex):
        return (H[r_ex] - RR[r_ex - l] * H[l]) % P

    D = {'R': 0, 'G': 1, 'B': 2}
    SS = [D[s] for s in S]
    HS = hash(SS)

    def count(TT):
        TS = hash(TT)
        res = 0
        for i in range(-(N - 1), N):
            if i < 0:
                a = h(HS, 0, N + i)
                b = h(TS, -i, N)
                if a == b:
                    res += 1
            else:
                a = h(TS, 0, N - i)
                b = h(HS, i, N)
                if a == b:
                    res += 1
        return res

    ans = 0
    D = {'R': 0, 'G': 2, 'B': 1}
    ans += count([D[t] for t in T])
    D = {'R': 2, 'G': 1, 'B': 0}
    ans += count([D[t] for t in T])
    D = {'R': 1, 'G': 0, 'B': 2}
    ans += count([D[t] for t in T])
    print(ans)

if __name__ == '__main__':
    main()

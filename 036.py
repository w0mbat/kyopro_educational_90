# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
N, Q = map(int, input().split())
XY = [tuple(map(int, input().split())) for _ in range(N)]
XX = [x + y for x, y in XY]
YY = [x - y for x, y in XY]
xdmin = min(XX)
xdmax = max(XX)
ydmin = min(YY)
ydmax = max(YY)
for _ in range(Q):
    q = int(input()) - 1
    x, y = XY[q]
    xd, yd = x + y, x - y
    ans = max(abs(xd - xdmin), abs(xd - xdmax), abs(yd - ydmin), abs(yd - ydmax))
    print(ans)

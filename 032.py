# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
N = int(input())
A = [tuple(map(int, input().split())) for _ in range(N)]
M = int(input())
D = [[0] * N for _ in range(N)]
for _ in range(M):
    x, y = map(int, input().split())
    x, y = x - 1, y - 1
    D[x][y] = 1
    D[y][x] = 1

def popcount(x):
    x = (x & 0x5555555555555555) + (x >> 1 & 0x5555555555555555)
    x = (x & 0x3333333333333333) + (x >> 2 & 0x3333333333333333)
    x = x + (x >> 4) & 0x0f0f0f0f0f0f0f0f
    x += x >> 8
    x += x >> 16
    x += x >> 32
    return x & 0x7f

INF = 10**18
dp = [[INF] * (1 << N) for _ in range(N)]
for i in range(N):
    dp[i][1 << i] = A[i][0]

for s in range(1, 1 << N):
    k = popcount(s)
    for j in range(N):
        if s & (1 << j): continue
        for i, Di in enumerate(D):
            if Di[j]: continue
            dp[j][s | (1 << j)] = min(dp[j][s | (1 << j)], dp[i][s] + A[j][k])

ans = min(dp[i][(1 << N) - 1] for i in range(N))
print(ans if ans < INF else -1)

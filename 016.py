# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
N = int(input())
C, B, A = sorted(map(int, input().split()))
M = 9999
ans = M
for a in range(min(M, N // A) + 1):
    n1 = N - a * A
    for b in range(min(M, n1 // B) + 1):
        n2 = n1 - b * B
        if n2 % C == 0:
            ans = min(ans, a + b + n2 // C)
print(ans)

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from operator import itemgetter
from typing import Iterable, List
import sys
def main():
    input = sys.stdin.readline
    N, M = map(int, input().split())
    CLR = []
    for _ in range(M):
        c, l, r = map(int, input().split())
        CLR.append((c, l - 1, r))
    CLR.sort(key=itemgetter(0))
    uf = UnionFindTree(N + 1)
    ans = 0
    for c, l, r in CLR:
        if uf.union(l, r):
            ans += c
    if uf.size(0) != N + 1: return -1
    return ans

class UnionFindTree:
    __slots__ = ('parent')

    def __init__(self, n: int) -> None:
        self.parent = [-1] * n

    def find(self, x: int) -> int:
        p = self.parent
        par, seq = p[x], []
        while par >= 0:
            seq.append(x)
            x, par = par, p[par]
        for c in seq: p[c] = x
        return x

    def union(self, x: int, y: int) -> bool:
        x, y, p = self.find(x), self.find(y), self.parent
        if x == y: return False
        if p[x] > p[y]: x, y = y, x
        p[x], p[y] = p[x] + p[y], x
        return True

    def same(self, x: int, y: int) -> bool:
        return self.find(x) == self.find(y)

    def size(self, x: int) -> int:
        return -self.parent[self.find(x)]

    def same_all(self, indices: Iterable[int]) -> bool:
        f, v = self.find, self.find(indices[0])
        return all(f(i) == v for i in indices)

    def groups(self) -> List[int]:
        return [i for i, p in enumerate(self.parent) if p < 0]

if __name__ == '__main__':
    print(main())

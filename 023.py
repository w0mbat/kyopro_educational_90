# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
MOD = 10**9 + 7
H, W = map(int, input().split())
C = []
push = C.extend
for _ in range(H):
    push(map(lambda c: 1 if c == '#' else 0, input()))
S = [[] for _ in range(W)]
Si = [dict() for _ in range(W)]
stk = []
push, pop = stk.append, stk.pop
for j in range(W):
    push((j, 0, 1))
    push((j, 1, 1))
while stk:
    j, v, l = pop()
    if l == W + 1:
        Si[j][v] = len(S[j])
        S[j].append(v)
        continue
    nj = (j + 1) % W
    push((nj, v << 1, l + 1))
    if l >= W and v & (1 << (W - 1)): continue
    if nj > 0 and v & 1: continue
    if nj < W - 1 and l >= W - 1 and v & (1 << (W - 2)): continue
    push((nj, (v << 1) + 1, l + 1))

mask = (1 << (W + 1)) - 1
N = max(len(s) for s in S)
UP = 1 << (W - 1)
UPLEFT = 1 << W if W >= 2 else 1 << (W + 1)
UPRIGHT = 1 << (W - 2) if W >= 2 else 1 << (W + 1)

nxt0 = [[[] for _ in range(N)] for _ in range(W)]
nxt1 = [[[] for _ in range(N)] for _ in range(W)]
for j in range(W):
    Sij = Si[j]
    for psi, ps in enumerate(S[(j - 1) % W]):
        nxt0[j][psi].append(Sij[(ps << 1) & mask])
        if ps & UP: continue
        if j > 0 and (ps & 1 or ps & UPLEFT): continue
        if j < W - 1 and ps & UPRIGHT: continue
        nxt1[j][psi].append(Sij[((ps << 1) & mask) + 1])

M = H * W
dp = [[0] * N for _ in range(M)]
dp[0][Si[0][0]] = 1
dp[0][Si[0][1]] = 0 if C[0] else 1
for p in range(1, M):
    j = p % W
    dpp = dp[p]
    dppp = dp[p - 1]
    for psi, ps in enumerate(S[(p - 1) % W]):
        pv = dppp[psi]
        if pv == 0: continue
        for si in nxt0[j][psi]:
            dpp[si] += pv
            dpp[si] %= MOD
        if C[p]: continue
        for si in nxt1[j][psi]:
            dpp[si] += pv
            dpp[si] %= MOD
ans = 0
for d in dp[M - 1]:
    ans = (ans + d) % MOD
print(ans)

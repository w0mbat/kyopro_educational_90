# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from decimal import Decimal, ROUND_HALF_UP
PHI = (Decimal(5).sqrt() + 1) / 2

def main(N):
    A = [-1] * N

    def query(i):
        if A[i] >= 0: return A[i]
        print('?', i + 1, flush=True)
        res = int(input())
        if res == -1:
            exit()
        A[i] = res
        return res

    def answer(i):
        x = query(i)
        print('!', x, flush=True)

    def rnd(d):
        return int(d.quantize(Decimal('0'), rounding=ROUND_HALF_UP))

    def small(l, r):
        n = r - l - 1
        if n >= 4: return False
        if n == 1:
            answer(l + 1)
        elif n == 2:
            n1, n2 = l + 1, l + 2
            if query(n1) < query(n2):
                answer(n2)
            else:
                answer(n1)
        else:
            n1, n2, n3 = l + 1, l + 2, l + 3
            if query(n1) > query(n2):
                answer(n1)
            elif query(n2) > query(n3):
                answer(n2)
            else:
                answer(n3)
        return True

    l = -1
    r = N
    if small(l, r): return
    x1 = rnd((PHI * l + r) / (1 + PHI))
    y1 = query(x1)
    x2 = rnd((l + PHI * r) / (1 + PHI))
    y2 = query(x2)
    while True:
        if small(l, r): return
        if y1 < y2:
            l = x1
            x1 = x2
            y1 = y2
            x2 = rnd((l + PHI * r) / (1 + PHI))
            y2 = query(x2)
        else:
            r = x2
            x2 = x1
            y2 = y1
            x1 = rnd((PHI * l + r) / (1 + PHI))
            y1 = query(x1)

if __name__ == '__main__':
    T = int(input())
    for _ in range(T):
        N = int(input())
        main(N)

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from functools import lru_cache
def main(N, A):
    @lru_cache(None)
    def f(l, r):
        if r <= l: return 0
        res = abs(A[l] - A[r]) + f(l + 1, r - 1)
        for i in range(l + 1, r + 1, 2):
            res = min(res, abs(A[l] - A[i]) + f(l + 1, i - 1) + f(i + 1, r))
        return res

    print(f(0, 2 * N - 1))

if __name__ == '__main__':
    N = int(input())
    *A, = map(int, input().split())
    main(N, A)

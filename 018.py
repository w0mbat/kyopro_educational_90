# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from math import atan2, cos, pi, sin, sqrt
T = int(input())
L, X, Y = map(int, input().split())
Q = int(input())
for _ in range(Q):
    e = int(input())
    t = e * 2 * pi / T
    y = -L * sin(t) / 2
    z = L * (1 - cos(t)) / 2
    d = sqrt(X * X + (y - Y) * (y - Y))
    w = atan2(z, d)
    print(w * 360 / 2 / pi)

# でつoO(YOU PLAY WITH THE CARDS YOU'RE DEALT..)
from math import gcd, sin, cos, radians, atan2
import sys
from typing import List, Tuple
def main():
    input = sys.stdin.readline
    N = int(input())
    P = [Vector(*map(int, input().split())) for _ in range(N)]
    cp = ConvexPolygon.convex_hull(P)
    print(cp.count_lattice_points() - N)

class GeoUtils:
    EPS = 10e-10

    @staticmethod
    def sgn(v: float) -> int:
        EPS = GeoUtils.EPS
        return -1 if v < -EPS else 1 if v > EPS else 0

class Vector:
    __slots__ = ('x', 'y')

    def __init__(self, x: float, y: float) -> None:
        self.x = x
        self.y = y

    def __repr__(self): return str((self.x, self.y))

    def __eq__(self, other):
        return (GeoUtils.sgn(self.x - other.x) == 0) and (
            GeoUtils.sgn(self.y - other.y) == 0)

    def __lt__(self, other):
        c = GeoUtils.sgn(self.x - other.x)
        return c < 0 if c != 0 else GeoUtils.sgn(self.y - other.y) < 0

    def __neg__(self): return Vector(-self.x, -self.y)
    def __hash__(self): return hash((self.x, self.y))

    def __bool__(self):
        return (GeoUtils.sgn(self.x) != 0) or (GeoUtils.sgn(self.y) != 0)

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        return self

    def __add__(self, other):
        new_obj = Vector(self.x, self.y)
        new_obj += other
        return new_obj
    __radd__ = __add__

    def __isub__(self, other):
        self.x -= other.x
        self.y -= other.y
        return self

    def __sub__(self, other):
        new_obj = Vector(self.x, self.y)
        new_obj -= other
        return new_obj

    def __rsub__(self, other):
        new_obj = Vector(other.x, other.y)
        new_obj -= self
        return new_obj

    def __imul__(self, multiplier: float):
        self.x *= multiplier
        self.y *= multiplier
        return self

    def __mul__(self, multiplier: float):
        new_obj = Vector(self.x, self.y)
        new_obj *= multiplier
        return new_obj
    __rmul__ = __mul__

    def __itruediv__(self, divisor: float):
        self.x /= divisor
        self.y /= divisor
        return self

    def __truediv__(self, divisor: float):
        new_obj = Vector(self.x, self.y)
        new_obj /= divisor
        return new_obj

    def xy(self) -> Tuple[float, float]:
        return (self.x, self.y)

    def length(self) -> float:
        return self.length_square()**0.5

    def length_square(self) -> float:
        x, y = self.x, self.y
        return x * x + y * y

    def rotate(self, degree: float):
        x, y, r = self.x, self.y, radians(degree)
        self.x = x * cos(r) - y * sin(r)
        self.y = x * sin(r) + y * cos(r)
        return self

    def dot(self, other) -> float:
        a, b, c, d = self.x, self.y, other.x, other.y
        return a * c + b * d

    def cross(self, other) -> float:
        a, b, c, d = self.x, self.y, other.x, other.y
        return a * d - b * c

    def cross_int(self, other) -> int:
        a, b, c, d = int(self.x), int(self.y), int(other.x), int(other.y)
        return a * d - b * c

    def angle_radians(self) -> float:
        return atan2(self.y, self.x)

    def distance_from(self, other) -> float:
        return (self - other).length()

    @staticmethod
    def three_points_relation(a, b, c) -> int:
        '''returns:
        +1: counter clockwise
        -1: clockwise
        +2: on the same line
        -2: on the same line or a==b
        0: on the same line or a==c or b==c
        '''
        b = b - a
        c = c - a
        bc = b.cross(c)
        if GeoUtils.sgn(bc) > 0: return 1
        if GeoUtils.sgn(bc) < 0: return -1
        if GeoUtils.sgn(b.dot(c)) < 0: return 2
        if GeoUtils.sgn(c.length() - b.length()) > 0: return -2
        return 0

class ConvexPolygon:
    def __init__(self, points: List[Vector]) -> None:
        self.P = points

    @classmethod
    def convex_hull(cls, points: List[Vector]):
        P = sorted(points)
        N = len(P)
        ch: List[Vector] = []
        for p in P:
            while len(ch) >= 2:
                if Vector.three_points_relation(ch[-2], ch[-1], p) > 0: break
                ch.pop()
            ch.append(p)
        t = len(ch)
        for i in range(N - 2, -1, -1):
            p = P[i]
            while len(ch) > t:
                if Vector.three_points_relation(ch[-2], ch[-1], p) > 0: break
                ch.pop()
            ch.append(p)
        return cls(ch[:-1])

    def area(self) -> float:
        P = self.P
        N = len(P)
        return sum(P[i].cross(P[(i + 1) % N]) for i in range(N)) / 2

    def area2_int(self) -> int:
        '''面積の2倍（すべての座標が整数であること）
        '''
        P = self.P
        N = len(P)
        return sum(P[i].cross_int(P[(i + 1) % N]) for i in range(N))

    def count_lattice_points_on_edge(self) -> int:
        '''辺上の格子点数（すべての座標が整数であること）
        '''
        P = self.P
        N = len(P)
        res = 0
        for i in range(N):
            dx = abs(P[i].x - P[(i + 1) % N].x)
            dy = abs(P[i].y - P[(i + 1) % N].y)
            res += gcd(dx, dy)
        return res

    def count_lattice_points(self) -> int:
        S2 = self.area2_int()
        b = self.count_lattice_points_on_edge()
        return (abs(S2) + b) // 2 + 1

if __name__ == '__main__':
    main()
